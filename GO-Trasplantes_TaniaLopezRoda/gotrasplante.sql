CREATE database if not exists gotrasplante;
--
use gotrasplante;
--
CREATE TABLE IF NOT EXISTS pacientes(
id int primary key auto_increment,
nombre varchar(50) not null,
apellido varchar(50) not null,
dni varchar(9) not null,
telefono long,
direccion varchar(150) not null,
fechaNac date,
grupoSanguineo varchar(20),
organoPendiente varchar(50)
);
--
CREATE TABLE IF NOT EXISTS donantes(
id int primary key auto_increment,
nombre varchar(50) not null,
apellido varchar(50) not null,
dni varchar(9) not null,
telefono long,
direccion varchar(150) not null,
fechaNac date,
grupoSanguineo varchar(20),
organo varchar(50),
tipoDonante varchar(50)
);
--
CREATE TABLE IF NOT EXISTS cirujanos(
id int primary key auto_increment,
nombre varchar(50) not null,
apellido varchar(50) not null,
dni varchar(9) not null,
telefono long,
direccion varchar(150) not null,
fechaNac date,
especialidad varchar(50)
);
--
CREATE TABLE IF NOT EXISTS operaciones(
id int primary key auto_increment,
nombre varchar(50) not null,
descripcion varchar(150),
sala varchar(50) not null,
fecha date,
duracion int,
organo varchar(50),
tipoAnestesia varchar(100),
id_cirujano INT NOT NULL,
id_paciente INT NOT NULL,
id_donante INT NOT NULL

);
--
alter table operaciones
add foreign key (id_cirujano) references cirujanos(id),
add foreign key (id_paciente) references pacientes(id),
add foreign key (id_donante) references donantes(id);
--
insert into pacientes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organoPendiente) values(1,'Pepe','Del Piero','88306928G',345850739,'C/Placido','1967-02-11','A-','corazón');
insert into pacientes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organoPendiente) values(2,'Alvaro','Sánchez','85985498H',348294047,'C/Ripa','1970-04-01','B-','hígado');
insert into pacientes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organoPendiente) values(3,'Macarena','Pérez','93849302Ñ',237489320,'C/San Pepe','1981-07-20','B-','páncreas');
insert into pacientes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organoPendiente) values(4,'Marta','Gómez','43839592M',374983291,'C/San Lorenzo','1991-05-23','AB-','intestino');
insert into pacientes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organoPendiente) values(5,'Ana','Ruíz','94739408J',948573829,'C/San Roque','1982-12-29','A+','páncreas');
insert into pacientes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organoPendiente) values(6,'Clara','Aguilar','93853772K',937445873,'C/Molina','1977-07-03','O+','pulmón');
insert into pacientes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organoPendiente) values(7,'Alberto','Alfranca','22461748P',986483271,'C/Pascual','1940-04-07','AB+','corazón');
insert into pacientes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organoPendiente) values(8,'Erika','López','90392251T',743235327,'C/San Juan','1965-09-26','A+','riñón');
insert into pacientes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organoPendiente) values(9,'Andrés','Campos','23145437D',847247784,'Plaza Castillo','1997-11-17','O-','riñón');
insert into pacientes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organoPendiente) values(10,'Laura','Escartín','57488355S',676382902,'C/Del Prado','1988-01-05','B+','pulmón');
--
insert into donantes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organo, tipoDonante) values(1,'Silvia','Alfranca','84930934M',343590235,'C/San Lucas','1989-03-04','B-','pulmón','Vivo emparentado');
insert into donantes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organo, tipoDonante) values(2,'Paula','Del Río','83924658O',890345228,'C/Martín','1975-02-22','A-','corazón','Vivo sin parentesco');
insert into donantes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organo, tipoDonante) values(3,'Irene','Aguilar','22347547U',783892543,'C/San Braulio','1983-08-15','AB+','corazón','Vivo emparentado');
insert into donantes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organo, tipoDonante) values(4,'Juan','García','17348894R',894234544,'C/Julián','1968-09-12','B+','pulmón','Vivo sin parentesco');
insert into donantes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organo, tipoDonante) values(5,'Carlos','González','90234056F',321436764,'Plaza Paraiso','1999-12-01','O+','pulmón','Fallecido');
insert into donantes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organo, tipoDonante) values(6,'Sergio','Fernández','22463711A',874673872,'C/Lagasca','1995-06-23','O-','riñón','Fallecido');
insert into donantes(id,nombre,apellido,dni, telefono,direccion,fechaNac, grupoSanguineo,organo, tipoDonante) values(7,'Beatriz','Pérez','77483943Z',8947576643,'Paseo de los Girasoles','1984-11-18','A+','páncreas','Vivo sin parentesco');
--
insert into cirujanos(id,nombre,apellido,dni, telefono,direccion,fechaNac, especialidad) values(1,'Mar','Santos','93784588D',787329439,'C/Morcillo','1975-08-14','Cirugía cardiaca');
insert into cirujanos(id,nombre,apellido,dni, telefono,direccion,fechaNac, especialidad) values(2,'Gonzalo','Rodriguez','89347821X',229993841,'C/Domingo','1989-02-04','Cirugía plástica');
insert into cirujanos(id,nombre,apellido,dni, telefono,direccion,fechaNac, especialidad) values(3,'Claudia','Riera','34782289V',783242744,'C/San Esteban','1983-06-06','Cirugía vascular');
insert into cirujanos(id,nombre,apellido,dni, telefono,direccion,fechaNac, especialidad) values(4,'Patricia','Díaz','22374391B',319438588,'C/Torrero','1969-01-26','Cirugía general');
insert into cirujanos(id,nombre,apellido,dni, telefono,direccion,fechaNac, especialidad) values(5,'Luis','Blanco','55688329C',874929844,'C/Manso','1980-11-24','Cirugía cardiotorácica');
--
insert into operaciones(id,nombre,descripcion,sala, fecha,duracion,organo, tipoAnestesia,id_cirujano,id_paciente,id_donante) values(1,'Operación de pulmón','Transplante de pulmón','1','2020-05-25',120,'pulmón','General',5,1,4);
insert into operaciones(id,nombre,descripcion,sala, fecha,duracion,organo, tipoAnestesia,id_cirujano,id_paciente,id_donante) values(2,'Operación de corazón','Transplante de corazón','2','2020-06-06',140,'corazón','General',1,1,2);
insert into operaciones(id,nombre,descripcion,sala, fecha,duracion,organo, tipoAnestesia,id_cirujano,id_paciente,id_donante) values(3,'Operación de riñón','Transplante de riñón','3','2020-06-14',90,'riñón','Regional',4,9,6);

