package com.tlopez.goTrasplantes.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Clase Login, donde creas la vista del login de la aplicación.
 * @author Tania López
 * @see JFrame
 */
public class Login extends JFrame{
    //Componentes de la ventana
    private JPanel panel1;
    public JFrame frame;
    public JPasswordField txtPassword;
    public JTextField txtUsuario;
    public JRadioButton rbAdministrador;
    public JRadioButton rbUsuario;
    public JRadioButton rbConsultor;

    public JButton btnIniciarSesion;
    public JButton btnCancelar;
    private JLabel lblPersona;

    public Vista vista;

    /**
     * Constructor de la clase Login, donde creo la ventana, añado los actions listeners de los botones de iniciar sesión
     * y cancelar, y añado imagenes a la interfaz
     */
    public Login(){
        frame = new JFrame("GO-Trasplantes--> Login");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(new Dimension(760,580));
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        Image img4=new ImageIcon("imagenes/vacuna.png").getImage();
       frame.setIconImage(img4);

        btnIniciarSesion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                login();
            }
        });
        btnCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelar();
            }
        });
        Image img=new ImageIcon("imagenes/persona.JPG").getImage();
        ImageIcon img2= new ImageIcon(img.getScaledInstance(300,250,Image.SCALE_SMOOTH));
        lblPersona.setIcon(img2);

    }


    /**
     * Método que inicia sesión, según el tipo de usuario que hayas escogido
     */
    private void login() {
        String usuario=txtUsuario.getText();
        String password= String.valueOf(txtPassword.getPassword());
        String tipoAdministrador= String.valueOf(rbAdministrador.isSelected());
        String tipoUsuario= String.valueOf(rbUsuario.isSelected());
        String tipoConsultor= String.valueOf(rbConsultor.isSelected());

        if(usuario.equals("administrador")&&password.equals("1")&&tipoAdministrador.equals("true")){
            Modelo modelo=new Modelo();
            vista=new Vista();
            vista.panel1.setVisible(true);
            Controlador controlador=new Controlador(modelo,vista);
            vista.frame.setTitle("GO-Trasplantes--> Vista de administrador");
            frame.dispose();
        }
        else if(usuario.equals("usuario")&&password.equals("2")&&tipoUsuario.equals("true")){
            Modelo modelo=new Modelo();
            vista=new Vista();
            vista.panel1.setVisible(true);
            vista.tabbedPane1.setEnabledAt(4,false);
            Controlador controlador=new Controlador(modelo,vista);
            frame.dispose();
        }
        else if(usuario.equals("consultor")&&password.equals("3")&&tipoConsultor.equals("true")){
            Consultor  consultor=new Consultor();
            frame.dispose();
        }
        else {
            JDialog error=new JDialog();
            error.setTitle("Error");
            JLabel label=new JLabel("Error al iniciar sesión");
            error.add(label);
            error.setVisible(true);
            error.pack();
            error.setLocationRelativeTo(null);
        }
    }

    /**
     * Método que cierra la aplicación
     */
    private void cancelar(){
        System.exit(0);
    }


}
