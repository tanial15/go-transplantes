package com.tlopez.goTrasplantes;
import com.tlopez.goTrasplantes.gui.*;
/**
 * Clase Main, que es la clase principal
 * @author Tania López
 */
public class Main {
    /**
     * Método main, que llama al Login, es decir a la aplicación
     * @param args de tipo String[]
     */
    public static void main(String[] args) {
        Login login=new Login();
    }
}
