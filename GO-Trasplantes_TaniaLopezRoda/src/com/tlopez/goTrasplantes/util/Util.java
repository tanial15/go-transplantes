package com.tlopez.goTrasplantes.util;

import javax.swing.*;

/**
 * Clase Util, donde se crean métodos para llamar en caso de error.
 * @author Tania López
 */
public class Util {
    /**
     * Método que muestra un mensaje de error con el texto recibido
     * @param message de tipo String, que es el texto del mensaje de error
     */
    public static void showErrorAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
    /**
     * Método me muestra un mensaje de aviso con el texto recibido
     * @param message de tipo String, que es el texto del mensaje de aviso
     */
    public static void showWarningAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Aviso", JOptionPane.WARNING_MESSAGE);
    }
    /**
     * Método me muestra un mensaje de información con el texto recibido
     * @param message de tipo String, que es el texto del mensaje de información
     */
    public static void showInfoAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Información", JOptionPane.INFORMATION_MESSAGE);
    }
}
