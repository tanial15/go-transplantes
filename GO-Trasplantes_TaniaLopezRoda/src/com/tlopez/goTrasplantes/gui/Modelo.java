package com.tlopez.goTrasplantes.gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;

/**
 * Clase Modelo, donde se encuentran los métodos que conectan y desconectan con la base de datos y los métodos de
 * insertar, modificar, listar, eliminar, buscar registros y vaciar tablas.
 * @author Tania López
 */
public class Modelo {
    //Campo
    private Connection conexion;

    /**
     * Método que conecta con la base de datos mysql
     */
    public void conectar() {

        try {
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/gotrasplante","root", "");
        } catch (SQLException sqle) {
            try {
                conexion = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/gotrasplante","root","");

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Método que lee el fichero de la base de datos
     * @return stringBuilder.toString()
     * @throws IOException
     */
    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("gotrasplante_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }

    /**
     * Método que desconecta de la base de datos mysql
     */
    public void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    //Pacientes

    /**
     * Método que sirve para insertar pacientes en la base de datos
     * @param nombre de tipo String
     * @param apellido de tipo String
     * @param dni de tipo String
     * @param tlf de tipo Long
     * @param direccion de tipo String
     * @param fechaNac de tipo LocalDate
     * @param grupoSangui de tipo String
     * @param organoPend de tipo String
     */
    public void insertarPaciente(String nombre, String apellido, String dni, long tlf, String direccion, LocalDate fechaNac,
                          String grupoSangui, String organoPend) {
        String sentenciaSql = "INSERT INTO pacientes (nombre,apellido,dni,telefono,direccion,fechaNac,grupoSanguineo,organoPendiente) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellido);
            sentencia.setString(3, dni);
            sentencia.setLong(4, tlf);
            sentencia.setString(5, direccion);
            sentencia.setDate(6, Date.valueOf(fechaNac));
            sentencia.setString(7,grupoSangui);
            sentencia.setString(8,organoPend);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Método que sirve para modificar pacientes de la base de datos
     * @param nombre de tipo String
     * @param apellido de tipo String
     * @param dni de tipo String
     * @param tlf de tipo Long
     * @param direccion de tipo String
     * @param fechaNac de tipo LocalDate
     * @param grupoSangui de tipo String
     * @param organoPend de tipo String
     * @param idPaciente de tipo int
     */
    public void modificarPaciente(String nombre, String apellido, String dni, long tlf, String direccion, LocalDate fechaNac,
                           String grupoSangui, String organoPend, int idPaciente) {

        String sentenciaSql = "UPDATE pacientes SET nombre = ?, apellido = ?, dni = ?, telefono = ?,direccion=?, fechaNac=?," +
                "grupoSanguineo=?, organoPendiente=? WHERE id = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellido);
            sentencia.setString(3, dni);
            sentencia.setLong(4, tlf);
            sentencia.setString(5, direccion);
            sentencia.setDate(6, Date.valueOf(fechaNac));
            sentencia.setString(7, grupoSangui);
            sentencia.setString(8, organoPend);
            sentencia.setInt(9, idPaciente);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método que sirve para eliminar pacientes de la base de datos
     * @param idPaciente de tipo int
     */
    public  void borrarPaciente(int idPaciente) {
            String sentenciaSql = "DELETE FROM pacientes WHERE id = ?";
            PreparedStatement sentencia = null;

            try {
                sentencia = conexion.prepareStatement(sentenciaSql);
                sentencia.setInt(1, idPaciente);
                sentencia.executeUpdate();
            } catch (SQLException sqle) {
                sqle.printStackTrace();
            } finally {
                if (sentencia != null)
                    try {
                        sentencia.close();
                    } catch (SQLException sqle) {
                        sqle.printStackTrace();
                    }
            }
        }

    /**
     * Método que lista todos los pacientes de la base de datos
     * @return resultado
     * @throws SQLException lanza excepción de fallo de conexión con la base de datos
     */
    public ResultSet consultarPaciente() throws SQLException {
        String sentenciaSql = "SELECT concat(id) as 'ID', concat(nombre) as 'Nombre', concat(apellido) as 'Apellido', " +
                "concat(dni) as 'DNI',concat(telefono) as 'Teléfono',concat(direccion) as 'Dirección',concat(fechaNac) as 'Fecha de nacimiento'," +
                " concat(grupoSanguineo) as 'Grupo Sanguíneo', concat(organoPendiente) as 'Órgano pendiente' FROM pacientes";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Método para buscar un registro de la tabla de pacientes de la base de datos
     * @param valor de tipo String, palabra que introduces para encontrar el registro
     * @return resultado
     * @throws SQLException lanza excepción de fallo de conexión con la base de datos
     */
    public ResultSet buscarPaciente(String valor) throws SQLException {

        String consulta = "SELECT * FROM pacientes WHERE CONCAT(id,nombre, apellido, dni, telefono,direccion,fechaNac,grupoSanguineo,organoPendiente)" +
                " LIKE '%" + valor + "%'";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método que elimina todos los registros de la tabla de pacientes
     * @return resultado
     * @throws SQLException lanza excepción de fallo de conexión con la base de datos
     */
    public int vaciarTablaPacientes() throws SQLException {
        String sentenciaSql = "DELETE FROM pacientes";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(sentenciaSql);
        int resultado= sentencia.executeUpdate();

        if (sentencia != null){

            sentencia.close();
        }
        return resultado;

    }

    //Donantes

    /**
     * Método que sirve para insertar donantes en la base de datos
     * @param nombre de tipo String
     * @param apellido de tipo String
     * @param dni de tipo String
     * @param tlf de tipo Long
     * @param direccion de tipo String
     * @param fechaNac de tipo LocalDate
     * @param grupoSangui de tipo String
     * @param organo de tipo String
     * @param tipoDonante de tipo String
     */
    public void insertarDonante(String nombre, String apellido, String dni, long tlf, String direccion, LocalDate fechaNac,
                          String grupoSangui, String organo, String tipoDonante) {
        String sentenciaSql = "INSERT INTO donantes (nombre,apellido,dni,telefono,direccion,fechaNac,grupoSanguineo,organo,tipoDonante) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellido);
            sentencia.setString(3, dni);
            sentencia.setLong(4, tlf);
            sentencia.setString(5, direccion);
            sentencia.setDate(6, Date.valueOf(fechaNac));
            sentencia.setString(7,grupoSangui);
            sentencia.setString(8,organo);
            sentencia.setString(9,tipoDonante);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método que sirve para modificar pacientes de la base de datos
     * @param nombre de tipo String
     * @param apellido de tipo String
     * @param dni de tipo String
     * @param tlf de tipo Long
     * @param direccion de tipo String
     * @param fechaNac de tipo LocalDate
     * @param grupoSangui de tipo String
     * @param organo de tipo String
     * @param tipoDonante de tipo String
     * @param idDonante de tipo int
     */
    public void modificarDonante(String nombre, String apellido, String dni, long tlf, String direccion, LocalDate fechaNac,
                          String grupoSangui, String organo, String tipoDonante, int idDonante) {

        String sentenciaSql = "UPDATE donantes SET nombre = ?, apellido = ?, dni = ?, telefono = ?,direccion=?, fechaNac=?," +
                "grupoSanguineo=?, organo=?, tipoDonante=? WHERE id = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellido);
            sentencia.setString(3, dni);
            sentencia.setLong(4, tlf);
            sentencia.setString(5, direccion);
            sentencia.setDate(6, Date.valueOf(fechaNac));
            sentencia.setString(7, grupoSangui);
            sentencia.setString(8, organo);
            sentencia.setString(9, tipoDonante);
            sentencia.setInt(10, idDonante);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método que sirve para eliminar donantes de la base de datos
     * @param idDonante de tipo int
     */
    public void borrarDonante(int idDonante) {
        String sentenciaSql = "DELETE FROM donantes WHERE id = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idDonante);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método que lista todos los donantes de la base de datos
     * @return resultado
     * @throws SQLException lanza excepción de fallo de conexión con la base de datos
     */
    public ResultSet consultarDonante() throws SQLException {
        String sentenciaSql = "SELECT concat(id) as 'ID', concat(nombre) as 'Nombre', concat(apellido) as 'Apellido', " +
                "concat(dni) as 'DNI',concat(telefono) as 'Teléfono',concat(direccion) as 'Dirección',concat(fechaNac) as 'Fecha de nacimiento'," +
                " concat(grupoSanguineo) as 'Grupo Sanguíneo', concat(organo) as 'Órgano a donar', concat(tipoDonante) as 'Tipo de donante' FROM donantes";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Método para buscar un registro de la tabla de donantes de la base de datos
     * @param valor de tipo String, palabra que introduces para encontrar el registro
     * @return resultado
     * @throws SQLException lanza excepción de fallo de conexión con la base de datos
     */
    public ResultSet buscarDonante(String valor) throws SQLException {

        String consulta = "SELECT * FROM donantes WHERE CONCAT(id,nombre, apellido, dni, telefono,direccion,fechaNac,grupoSanguineo,organo,tipoDonante)" +
                " LIKE '%" + valor + "%'";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    /**
     * Método que elimina todos los registros de la tabla de donantes
     * @return resultado
     * @throws SQLException lanza excepción de fallo de conexión con la base de datos
     */
    public int vaciarTablaDonantes() throws SQLException {
        String sentenciaSql = "DELETE FROM donantes";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(sentenciaSql);
        int resultado= sentencia.executeUpdate();

        if (sentencia != null){

            sentencia.close();
        }
        return resultado;

    }

    //Cirujano

    /**
     * Método que sirve para insertar cirujanos en la base de datos
     * @param nombre de tipo String
     * @param apellido de tipo String
     * @param dni de tipo String
     * @param tlf de tipo Long
     * @param direccion de tipo String
     * @param fechaNac de tipo LocalDate
     * @param especialidad de tipo String
     */
    public void insertarCirujano(String nombre, String apellido, String dni, long tlf, String direccion, LocalDate fechaNac,
                         String especialidad) {
        String sentenciaSql = "INSERT INTO cirujanos (nombre,apellido,dni,telefono,direccion,fechaNac,especialidad) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellido);
            sentencia.setString(3, dni);
            sentencia.setLong(4, tlf);
            sentencia.setString(5, direccion);
            sentencia.setDate(6, Date.valueOf(fechaNac));
            sentencia.setString(7,especialidad);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método que sirve para modificar cirujanos de la base de datos
     * @param nombre de tipo String
     * @param apellido de tipo String
     * @param dni de tipo String
     * @param tlf de tipo Long
     * @param direccion de tipo String
     * @param fechaNac de tipo LocalDate
     * @param especialidad de tipo String
     * @param idCirujano de tipo int
     */
    public void modificarCirujano(String nombre, String apellido, String dni, long tlf, String direccion, LocalDate fechaNac,
                           String especialidad, int idCirujano) {

        String sentenciaSql = "UPDATE cirujanos SET nombre = ?, apellido = ?, dni = ?, telefono = ?,direccion=?, fechaNac=?," +
                "especialidad=? WHERE id = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellido);
            sentencia.setString(3, dni);
            sentencia.setLong(4, tlf);
            sentencia.setString(5, direccion);
            sentencia.setDate(6, Date.valueOf(fechaNac));
            sentencia.setString(7, especialidad);
            sentencia.setInt(8, idCirujano);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método que sirve para eliminar cirujanos de la base de datos
     * @param idCirujano de tipo int
     */
    public void borrarCirujano(int idCirujano) {
        String sentenciaSql = "DELETE FROM cirujanos WHERE id = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idCirujano);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Método que lista todos los cirujanos de la base de datos
     * @return resultado
     * @throws SQLException lanza excepción de fallo de conexión con la base de datos
     */
    public ResultSet consultarCirujano() throws SQLException {
        String sentenciaSql = "SELECT concat(id) as 'ID', concat(nombre) as 'Nombre', concat(apellido) as 'Apellido', " +
                "concat(dni) as 'DNI',concat(telefono) as 'Teléfono',concat(direccion) as 'Dirección',concat(fechaNac) as 'Fecha de nacimiento'," +
                " concat(especialidad) as 'Especialidad' FROM cirujanos";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }
    /**
     * Método para buscar un registro de la tabla de cirujanos de la base de datos
     * @param valor de tipo String, palabra que introduces para encontrar el registro
     * @return resultado
     * @throws SQLException lanza excepción de fallo de conexión con la base de datos
     */
    public ResultSet buscarCirujano(String valor) throws SQLException {

        String consulta = "SELECT * FROM cirujanos WHERE CONCAT(id,nombre, apellido, dni, telefono,direccion,fechaNac,especialidad)" +
                " LIKE '%" + valor + "%'";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }
    /**
     * Método que elimina todos los registros de la tabla de cirujanos
     * @return resultado
     * @throws SQLException lanza excepción de fallo de conexión con la base de datos
     */
    public int vaciarTablaCirujanos() throws SQLException {
        String sentenciaSql = "DELETE FROM cirujanos";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(sentenciaSql);
        int resultado= sentencia.executeUpdate();

        if (sentencia != null){

            sentencia.close();
        }
        return resultado;

    }

    //Operacion

    /**
     * Método que sirve para insertar operaciones en la base de datos
     * @param nombre de tipo String
     * @param descripcion de tipo String
     * @param sala de tipo String
     * @param fecha de tipo LocalDate
     * @param duracion de tipo int
     * @param organo de tipo String
     * @param tipoAnestesia de tipo String
     * @param cirujano de tipo int
     * @param paciente de tipo int
     * @param donante de tipo int
     */
    public void insertarOperacion(String nombre, String descripcion, String sala, LocalDate fecha,int duracion, String organo,
                           String tipoAnestesia, String cirujano,String paciente, String donante) {
        String sentenciaSql = "INSERT INTO operaciones (nombre,descripcion,sala,fecha,duracion,organo,tipoAnestesia," +
                " id_cirujano,id_paciente,id_donante) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        int idcirujano = Integer.valueOf(cirujano.split(" ")[0]);
        int idpaciente= Integer.valueOf(paciente.split(" ")[0]);
        int iddonante=Integer.valueOf(donante.split(" ")[0]);
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, descripcion);
            sentencia.setString(3, sala);
            sentencia.setDate(4, Date.valueOf(fecha));
            sentencia.setInt(5,duracion);
            sentencia.setString(6, organo);
            sentencia.setString(7, tipoAnestesia);
            sentencia.setInt(8,idcirujano);
            sentencia.setInt(9,idpaciente);
            sentencia.setInt(10,iddonante);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método que sirve para modificar operaciones de la base de datos
     * @param nombre de tipo String
     * @param descripcion de tipo String
     * @param sala de tipo String
     * @param fecha de tipo LocalDate
     * @param duracion de tipo int
     * @param organo de tipo String
     * @param tipoAnestesia de tipo String
     * @param cirujano de tipo int
     * @param paciente de tipo int
     * @param donante de tipo int
     * @param idOperacion de tipo int
     */
    public void modificarOperacion(String nombre, String descripcion, String sala, LocalDate fecha,int duracion, String organo,
                            String tipoAnestesia, String cirujano,String paciente, String donante, int idOperacion) {

        String sentenciaSql = "UPDATE operaciones SET nombre = ?, descripcion = ?, sala = ?, fecha = ?,duracion=?, organo=?," +
                "tipoAnestesia=?, id_cirujano=?, id_paciente=?,id_donante=? WHERE id = ?";
        PreparedStatement sentencia = null;

        int idcirujano = Integer.valueOf(cirujano.split(" ")[0]);
        int idpaciente= Integer.valueOf(paciente.split(" ")[0]);
        int iddonante=Integer.valueOf(donante.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, descripcion);
            sentencia.setString(3, sala);
            sentencia.setDate(4, Date.valueOf(fecha));
            sentencia.setInt(5,duracion);
            sentencia.setString(6, organo);
            sentencia.setString(7, tipoAnestesia);
            sentencia.setInt(8,idcirujano);
            sentencia.setInt(9,idpaciente);
            sentencia.setInt(10,iddonante);
            sentencia.setInt(11, idOperacion);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Método que sirve para eliminar operaciones de la base de datos
     * @param idOperacion de tipo int
     */
    public void borrarOperacion(int idOperacion) {
        String sentenciaSql = "DELETE FROM operaciones WHERE id = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idOperacion);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    /**
     * Método que lista todos las operaciones de la base de datos
     * @return resultado
     * @throws SQLException lanza excepción de fallo de conexión con la base de datos
     */
    public ResultSet consultarOperacion() throws SQLException {
        String sentenciaSql = "SELECT concat(o.id) as 'ID', concat(o.nombre) as 'Nombre', concat(o.descripcion) as 'Descripción', " +
                "concat(o.sala) as 'Sala',concat(o.fecha) as 'Fecha',concat(o.duracion) as 'Duración',concat(o.organo) as 'Órgano'," +
                " concat(o.tipoAnestesia) as 'Tipo de anestesia',concat(c.id, ' - ',c.nombre,' ',c.apellido) as 'Cirujano'," +
                "concat(p.id, ' - ', p.nombre,' ',p.apellido) as 'Paciente', concat(d.id, ' - ', d.nombre,' ',d.apellido) as 'Donante'" +
                " FROM operaciones as o inner join cirujanos c on o.id_cirujano = c.id " +
                "inner join pacientes p on o.id_paciente = p.id inner join donantes d on o.id_donante = d.id";

        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }
    /**
     * Método para buscar un registro de la tabla de operaciones de la base de datos
     * @param valor de tipo String, palabra que introduces para encontrar el registro
     * @return resultado
     * @throws SQLException lanza excepción de fallo de conexión con la base de datos
     */
    public ResultSet buscarOperacion(String valor) throws SQLException {

        String consulta = "SELECT * FROM operaciones WHERE CONCAT(id,nombre, descripcion,sala,fecha,duracion,organo,tipoAnestesia,id_cirujano,id_paciente,id_donante)" +
                " LIKE '%" + valor + "%'";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }
    /**
     * Método que elimina todos los registros de la tabla de operaciones
     * @return resultado
     * @throws SQLException lanza excepción de fallo de conexión con la base de datos
     */
    public int vaciarTablaOperaciones() throws SQLException {
        String sentenciaSql = "DELETE FROM operaciones";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(sentenciaSql);
        int resultado= sentencia.executeUpdate();

        if (sentencia != null){

            sentencia.close();
        }
        return resultado;

    }
}