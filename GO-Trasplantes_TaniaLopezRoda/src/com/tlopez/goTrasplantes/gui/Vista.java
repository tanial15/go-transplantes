package com.tlopez.goTrasplantes.gui;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Clase Vista, que contiene todos los componentes de la interfaz del usuario y del administrador.
 * @author Tania López
 * @see JFrame
 */
public class Vista extends JFrame {
    //Componentes generales
    public JFrame frame;
    public JPanel panel1;
    public JTabbedPane tabbedPane1;
    //Pacientes
    public JTextField txtNombrePaciente;
    public JTextField txtApellidoPaciente;
    public JTextField txtDniPaciente;
    public JTextField txtTelefonoPaciente;
    public JTextField txtDireccionPaciente;
    public JComboBox cbGrupoSangPaciente;
    public DatePicker dateFechaNacPaciente;
    public JComboBox cbOrganoPendPaciente;
    public JTextField txtBuscarPacientes;
    public JButton btnBuscarPacientes;
    public JButton btnAddPacientes;
    public JButton btnModificarPacientes;
    public  JButton btnEliminarPacientes;
    public JButton btnListarPacientes;
    public JTable tablePacientes;
    public JLabel lblAccionPacientes;
    //Donantes
    public JTextField txtNombreDonante;
    public JTextField txtApellidosDonante;
    public JTextField txtDniDonante;
    public JTextField txtTelefonoDonante;
    public JTextField txtDireccionDonante;
    public JComboBox cbGrupoSangDonante;
    public DatePicker dateFechaNacDonante;
    public JComboBox cbTipoDonante;
    public JComboBox cbOrganoDonante;
    public JButton btnAddDonante;
    public JButton btnBuscarDonante;
    public JTextField txtBuscarDonantes;
    public JButton btnListarDonante;
    public JButton btnModificarDonante;
    public JButton btnEliminarDonante;
    public JTable tableDonantes;
    public JLabel lblAccionDonante;
    //Cirujanos
    public JTextField txtNombreCirujano;
    public JTextField txtApellidosCirujano;
    public JTextField txtDniCirujano;
    public JTextField txtTelefonoCirujano;
    public JTextField txtDireccionCirujano;
    public DatePicker dateFechaNacCirujano;
    public JComboBox cbEspecialidadCirujano;
    public JButton btnAddCirujano;
    public JButton btnBuscarCirujano;
    public JTextField txtBuscarCirujano;
    public JButton btnListarCirujano;
    public JButton btnModificarCirujano;
    public JButton btnEliminarCirujano;
    public JTable tableCirujanos;
    public JLabel lblAccionCirujano;
    //Operaciones
    public JTextField txtNombreOperacion;
    public JComboBox cbSalaOperacion;
    public JTextField txtDescripcionOperacion;
    public JTextField txtDuraccionOperacion;
    public JComboBox cbAnestesiaOperacion;
    public DatePicker dateFechaOperacion;
    public JComboBox cbOrganoOperacion;
    public JComboBox cbPacienteOperacion;
    public JComboBox cbDonanteOperacion;
    public JComboBox cbCirujanoOperacion;
    public JButton btnAddOperacion;
    public JButton btnBuscarOperacion;
    public JTextField txtBuscarOperacion;
    public JButton btnListarOperacion;
    public JButton btnModificarOperacion;
    public JButton btnEliminarOperacion;
    public JTable tableOperacion;
    public JLabel lblAccionOperacion;

    //Administracion
    public JButton btnLimpiarO;
    public JButton btnLimpiarP;
    public JButton btnLimpiarD;
    public JButton btnLimpiarC;
    public JLabel lblAdvice;
    public JLabel lblAviso;

     //Otras
     public JButton btnSalir;

    //Default Table Models
    public DefaultTableModel dtmPacientes;
    public  DefaultTableModel dtmDonantes;
    public  DefaultTableModel dtmCirujanos;
    public DefaultTableModel dtmOperaciones;

     //Menu
     public JMenuItem itemConectar;
     public JMenuItem itemSalir;
     public JMenuItem itemDesconectar;

    /**
     * Constructor de Vista, que llama al método que inicializa la ventana
     */
    public Vista(){
        initFrame();
    }

    /**
     * Método, que inicializa la ventana del usuario y administrador, llama al método que inicializa las tablas y al
     * método que crea el menú. Además, adjunta iconos de imagen a los diverson botones
     */
    private void initFrame() {
        frame = new JFrame("GO-Trasplantes--> Vista de usuario");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        Image img9=new ImageIcon("imagenes/vacuna.png").getImage();
        frame.setIconImage(img9);

        setTableModels();
        crearMenu();
        //Icono de añadir
        Image img=new ImageIcon("imagenes/add.png").getImage();
        ImageIcon imgAdd= new ImageIcon(img.getScaledInstance(30,30,Image.SCALE_SMOOTH));
        btnAddPacientes.setIcon(imgAdd);
        btnAddDonante.setIcon(imgAdd);
        btnAddCirujano.setIcon(imgAdd);
        btnAddOperacion.setIcon(imgAdd);
        //Icono de modificar
        Image img2=new ImageIcon("imagenes/fix.png").getImage();
        ImageIcon imgFix= new ImageIcon(img2.getScaledInstance(30,30,Image.SCALE_SMOOTH));
        btnModificarPacientes.setIcon(imgFix);
        btnModificarDonante.setIcon(imgFix);
        btnModificarCirujano.setIcon(imgFix);
        btnModificarOperacion.setIcon(imgFix);
        //Icono buscar
        Image img3=new ImageIcon("imagenes/lupa.png").getImage();
        ImageIcon imgBuscar= new ImageIcon(img3.getScaledInstance(30,30,Image.SCALE_SMOOTH));
        btnBuscarPacientes.setIcon(imgBuscar);
        btnBuscarDonante.setIcon(imgBuscar);
        btnBuscarCirujano.setIcon(imgBuscar);
        btnBuscarOperacion.setIcon(imgBuscar);
        //Icono eliminar
        Image img4=new ImageIcon("imagenes/bin.png").getImage();
        ImageIcon imgBin= new ImageIcon(img4.getScaledInstance(30,30,Image.SCALE_SMOOTH));
        btnEliminarPacientes.setIcon(imgBin);
        btnEliminarDonante.setIcon(imgBin);
        btnEliminarCirujano.setIcon(imgBin);
        btnEliminarOperacion.setIcon(imgBin);
        //Icono listar
        Image img5=new ImageIcon("imagenes/tabla.png").getImage();
        ImageIcon imgListar= new ImageIcon(img5.getScaledInstance(30,30,Image.SCALE_SMOOTH));
        btnListarPacientes.setIcon(imgListar);
        btnListarDonante.setIcon(imgListar);
        btnListarCirujano.setIcon(imgListar);
        btnListarOperacion.setIcon(imgListar);
        //Salir
        Image img6=new ImageIcon("imagenes/close.png").getImage();
        ImageIcon imgSalir= new ImageIcon(img6.getScaledInstance(30,30,Image.SCALE_SMOOTH));
        btnSalir.setIcon(imgSalir);
        //Limpiar
        Image img7=new ImageIcon("imagenes/clean.png").getImage();
        ImageIcon imgClean= new ImageIcon(img7.getScaledInstance(30,30,Image.SCALE_SMOOTH));
        btnLimpiarC.setIcon(imgClean);
        btnLimpiarD.setIcon(imgClean);
        btnLimpiarO.setIcon(imgClean);
        btnLimpiarP.setIcon(imgClean);
        //Aviso
        Image img8=new ImageIcon("imagenes/advice.png").getImage();
        ImageIcon imgAviso = new ImageIcon(img8.getScaledInstance(70,70,Image.SCALE_SMOOTH));
        lblAdvice.setIcon(imgAviso);

        Image img10=new ImageIcon("imagenes/asteriscos.png").getImage();
        ImageIcon imgAdvertencia = new ImageIcon(img10.getScaledInstance(800,70,Image.SCALE_SMOOTH));
        lblAviso.setIcon(imgAdvertencia);

        //Tamaño de la letra de los nombres de las columnas
        tablePacientes.getTableHeader().setFont(new Font("Times New Roman ",Font.BOLD,18));
        tableDonantes.getTableHeader().setFont(new Font("Times New Roman ",Font.BOLD,18));
        tableCirujanos.getTableHeader().setFont(new Font("Times New Roman ",Font.BOLD,18));
        tableOperacion.getTableHeader().setFont(new Font("Times New Roman ",Font.BOLD,18));
    }

    /**
     * Método que inicializa las tablas con sus DefaultTableModel
     */
    private void setTableModels() {
        this.dtmPacientes=new DefaultTableModel();
        this.tablePacientes.setModel(dtmPacientes);

        this.dtmDonantes=new DefaultTableModel();
        this.tableDonantes.setModel(dtmDonantes);

        this.dtmCirujanos=new DefaultTableModel();
        this.tableCirujanos.setModel(dtmCirujanos);

        this.dtmOperaciones=new DefaultTableModel();
        this.tableOperacion.setModel(dtmOperaciones);
    }

    /**
     * Método que crea el menú, con las opciones de conectar, desconectar y salir de la aplicación
     */
    private void crearMenu(){
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemDesconectar);
        menuArchivo.add(itemSalir);

        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);
        frame.setJMenuBar(barraMenu);
    }

}
