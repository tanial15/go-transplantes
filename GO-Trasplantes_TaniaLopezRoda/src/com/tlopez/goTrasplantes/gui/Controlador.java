package com.tlopez.goTrasplantes.gui;

import com.tlopez.goTrasplantes.util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.Vector;

/**
 * Clase Controlador, donde das funcionalidad a la aplicación de las vistas de administrador y usuario.
 * @author Tania López
 * @see ListSelectionListener
 * @see WindowListener
 * @see ActionListener
 * @see ItemListener
 */
public class Controlador implements WindowListener, ActionListener, ItemListener, ListSelectionListener {
    //Campos
    private Modelo modelo;
    private Vista vista;
    public boolean refrescar;

    /**
     * Contructor de Controlador, donde inicializas modelo y vista, te conectas automaticamente a la base de datos,
     * llamas al método de refrescar todas las tablas, y donde añades los listeners
     * @param modelo de tipo Modelo
     * @param vista de tipo Vista
     */
    public Controlador(Modelo modelo,Vista vista) {
        this.modelo = modelo;
        this.vista = vista;

        modelo.conectar();

        addActionListeners(this);
        addItemListener(this);
        addWindowListener(this);

        refrescarTodo();
    }

    /**
     * Método que añade los itemListeners
     * @param controlador de tipo Controlador
     */
    private void addItemListener(Controlador controlador) {}

    /**
     * Método que añade los WindowListener a la ventana
     * @param listener WindowListener
     */
    private void addWindowListener(WindowListener listener) {
        vista.addWindowListener(listener);
    }
    /**
     * Método que añade los ActionListener a los botones
     * @param listener ActionListener
     */
    private void addActionListeners(ActionListener listener) {
        //Menu
        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        //Paciente
        vista.btnAddPacientes.addActionListener(listener);
        vista.btnModificarPacientes.addActionListener(listener);
        vista.btnEliminarPacientes.addActionListener(listener);
        vista.btnListarPacientes.addActionListener(listener);
        vista.btnBuscarPacientes.addActionListener(listener);
        //Donante
        vista.btnAddDonante.addActionListener(listener);
        vista.btnModificarDonante.addActionListener(listener);
        vista.btnEliminarDonante.addActionListener(listener);
        vista.btnListarDonante.addActionListener(listener);
        vista.btnBuscarDonante.addActionListener(listener);
        //Cirujano
        vista.btnAddCirujano.addActionListener(listener);
        vista.btnModificarCirujano.addActionListener(listener);
        vista.btnEliminarCirujano.addActionListener(listener);
        vista.btnListarCirujano.addActionListener(listener);
        vista.btnBuscarCirujano.addActionListener(listener);
        //Operacion
        vista.btnAddOperacion.addActionListener(listener);
        vista.btnModificarOperacion.addActionListener(listener);
        vista.btnEliminarOperacion.addActionListener(listener);
        vista.btnListarOperacion.addActionListener(listener);
        vista.btnBuscarOperacion.addActionListener(listener);
        //Salir
        vista.btnSalir.addActionListener(listener);

        //Administracion
        vista.btnLimpiarO.addActionListener(listener);
        vista.btnLimpiarP.addActionListener(listener);
        vista.btnLimpiarC.addActionListener(listener);
        vista.btnLimpiarD.addActionListener(listener);
    }

    /**
     * Método que llama a todos los métodos de refrescar pacientes, donantes, cirujanos y operaciones
     */
    private void refrescarTodo() {
        refrescarPacientes();
        refrescarDonantes();
        refrescarCirujanos();
        refrescarOperaciones();
        refrescar = false;
    }

    /**
     * Método que actualiza los datos de la tabla de pacientes y el comboBox de pacientes que se encuentra en operaciones
     */
    private void refrescarPacientes() {
        try {
            vista.tablePacientes.setModel(construirTableModelPacientes(modelo.consultarPaciente()));
            vista.cbPacienteOperacion.removeAllItems();
            for(int i = 0; i < vista.dtmPacientes.getRowCount(); i++) {
                vista.cbPacienteOperacion.addItem(vista.dtmPacientes.getValueAt(i, 0)+" - "+
                        vista.dtmPacientes.getValueAt(i, 1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Método que actualiza los datos de la tabla de donantes y el comboBox de donantes que se encuentra en operaciones
     */
    private void refrescarDonantes() {
        try {
            vista.tableDonantes.setModel(construirTableModelDonantes(modelo.consultarDonante()));
            vista.cbDonanteOperacion.removeAllItems();
            for(int i = 0; i < vista.dtmDonantes.getRowCount(); i++) {
                vista.cbDonanteOperacion.addItem(vista.dtmDonantes.getValueAt(i, 0)+" - "+
                        vista.dtmDonantes.getValueAt(i, 1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Método que actualiza los datos de la tabla de cirujanos y el comboBox de cirujanos que se encuentra en operaciones
     */
    private void refrescarCirujanos() {
        try {
            vista.tableCirujanos.setModel(construirTableModelCirujanos(modelo.consultarCirujano()));
            vista.cbCirujanoOperacion.removeAllItems();
            for(int i = 0; i < vista.dtmCirujanos.getRowCount(); i++) {
                vista.cbCirujanoOperacion.addItem(vista.dtmCirujanos.getValueAt(i, 0)+" - "+
                        vista.dtmCirujanos.getValueAt(i, 1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Método que actualiza los datos de la tabla de operaciones
     */
    private void refrescarOperaciones() {
        try {
            vista.tableOperacion.setModel(construirTableModelOperaciones(modelo.consultarOperacion()));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método que construye la tabla de pacientes
     * @param rs de tipo ResultSet
     * @return vista.dtmPacientes
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelPacientes(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));

        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmPacientes.setDataVector(data, columnNames);

        return vista.dtmPacientes;

    }
    /**
     * Método que construye la tabla de donantes
     * @param rs de tipo ResultSet
     * @return vista.dtmDonantes
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelDonantes(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmDonantes.setDataVector(data, columnNames);

        return vista.dtmDonantes;

    }
    /**
     * Método que construye la tabla de cirujanos
     * @param rs de tipo ResultSet
     * @return vista.dtmCirujanos
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelCirujanos(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmCirujanos.setDataVector(data, columnNames);

        return vista.dtmCirujanos;

    }
    /**
     * Método que construye la tabla de operaciones
     * @param rs de tipo ResultSet
     * @return vista.dtmOperaciones
     * @throws SQLException
     */
    private DefaultTableModel construirTableModelOperaciones(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmOperaciones.setDataVector(data, columnNames);

        return vista.dtmOperaciones;

    }

    /**
     * Método que recorre el vector de datos
     * @param rs de tipo ResultSet
     * @param columnCount de tipo int
     * @param data de tipo Vector<Vector<Object>>
     * @throws SQLException
     */
    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    /**
     * Método implementado por ActionListener, donde asigno las acciones a los diferentes botones
     * @param e de tipo ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "AñadirPacientes": {
                try {
                    if (comprobarPacienteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablePacientes.clearSelection();
                    } else {
                        modelo.insertarPaciente(vista.txtNombrePaciente.getText(),vista.txtApellidoPaciente.getText(),
                                vista.txtDniPaciente.getText(), Long.parseLong(vista.txtTelefonoPaciente.getText()),
                                vista.txtDireccionPaciente.getText(), vista.dateFechaNacPaciente.getDate(),
                                (String)vista.cbGrupoSangPaciente.getSelectedItem(),(String) vista.cbOrganoPendPaciente.getSelectedItem());

                        if(vista.txtDniPaciente.getText().length()!=9){
                            Util.showErrorAlert("El campo del DNI exige 9 carácteres");
                        }

                        refrescarPacientes();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablePacientes.clearSelection();
                }
                borrarCamposPaciente();
            }
                break;
            case "ModificarPacientes":{
                try {
                    if (comprobarPacienteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablePacientes.clearSelection();
                    } else {
                        modelo.modificarPaciente(vista.txtNombrePaciente.getText(),vista.txtApellidoPaciente.getText(),
                                vista.txtDniPaciente.getText(), Long.parseLong(vista.txtTelefonoPaciente.getText()),
                                vista.txtDireccionPaciente.getText(), vista.dateFechaNacPaciente.getDate(),
                                (String)vista.cbGrupoSangPaciente.getSelectedItem(), (String) vista.cbOrganoPendPaciente.getSelectedItem(),
                                Integer.parseInt((String) vista.tablePacientes.getValueAt(vista.tablePacientes.getSelectedRow(), 0)));

                        if(vista.txtDniPaciente.getText().length()!=9){
                            Util.showErrorAlert("El campo del DNI exige 9 carácteres");
                        }
                        refrescarPacientes();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablePacientes.clearSelection();
                }
                borrarCamposPaciente();
            }
                break;
            case "EliminarPacientes":
                    modelo.borrarPaciente(Integer.parseInt((String) vista.tablePacientes.getValueAt(vista.tablePacientes.getSelectedRow(), 0)));
                    borrarCamposPaciente();
                    refrescarPacientes();
                break;
            case "ListarPacientes":
                refrescarPacientes();
                break;
            case "BuscarPacientes":
                try {
                    if(vista.txtBuscarPacientes.getText().equalsIgnoreCase("")) {
                        cargarFilasPaciente(modelo.buscarPaciente(""));
                        vista.txtBuscarPacientes.setText("");
                    }
                    else {
                        cargarFilasPaciente(modelo.buscarPaciente(vista.txtBuscarPacientes.getText()));
                        vista.txtBuscarPacientes.setText("");
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                borrarCamposPaciente();
                vista.lblAccionPacientes.setText("Resultados Encontrados");
                vista.lblAccionPacientes.setForeground(new Color(31, 138, 19));

                break;
            case "AñadirDonantes":{
                try {
                    if (comprobarDonanteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tableDonantes.clearSelection();
                    } else {
                        modelo.insertarDonante(vista.txtNombreDonante.getText(),vista.txtApellidosDonante.getText(),
                                vista.txtDniDonante.getText(), Long.parseLong(vista.txtTelefonoDonante.getText()),
                                vista.txtDireccionDonante.getText(), vista.dateFechaNacDonante.getDate(),
                                (String)vista.cbGrupoSangDonante.getSelectedItem(), (String)vista.cbOrganoDonante.getSelectedItem(),
                                (String)vista.cbTipoDonante.getSelectedItem());

                        if(vista.txtDniDonante.getText().length()!=9){
                            Util.showErrorAlert("El campo del DNI exige 9 carácteres");
                        }
                        refrescarDonantes();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tableDonantes.clearSelection();
                }
                borrarCamposDonante();
            }
                break;
            case "ModificarDonantes":{
                try {
                    if (comprobarDonanteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tableDonantes.clearSelection();
                    } else {
                        modelo.modificarDonante(vista.txtNombreDonante.getText(),vista.txtApellidosDonante.getText(),
                                vista.txtDniDonante.getText(), Long.parseLong(vista.txtTelefonoDonante.getText()),
                                vista.txtDireccionDonante.getText(), vista.dateFechaNacDonante.getDate(),
                                (String)vista.cbGrupoSangDonante.getSelectedItem(), (String)vista.cbOrganoDonante.getSelectedItem(),
                                (String)vista.cbTipoDonante.getSelectedItem(),
                                Integer.parseInt((String) vista.tableDonantes.getValueAt(vista.tableDonantes.getSelectedRow(), 0)));

                        if(vista.txtDniDonante.getText().length()!=9){
                            Util.showErrorAlert("El campo del DNI exige 9 carácteres");
                        }
                        refrescarDonantes();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tableDonantes.clearSelection();
                }
                borrarCamposDonante();
            }
                break;
            case "EliminarDonantes":
                modelo.borrarDonante(Integer.parseInt((String) vista.tableDonantes.getValueAt(vista.tableDonantes.getSelectedRow(), 0)));
                borrarCamposDonante();
                refrescarDonantes();
                break;
            case "ListarDonantes":
                refrescarDonantes();
                break;
            case "BuscarDonantes":
                try {
                    if(vista.txtBuscarDonantes.getText().equalsIgnoreCase("")) {
                        cargarFilasDonantes(modelo.buscarDonante(""));
                        vista.txtBuscarDonantes.setText("");
                    }
                    else {
                        cargarFilasDonantes(modelo.buscarDonante(vista.txtBuscarDonantes.getText()));
                        vista.txtBuscarDonantes.setText("");
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                borrarCamposDonante();
                vista.lblAccionDonante.setText("Resultados Encontrados");
                vista.lblAccionDonante.setForeground(new Color(31, 138, 19));

                break;
            case "AñadirCirujanos":{
                try {
                    if (comprobarCirujanoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tableCirujanos.clearSelection();
                    } else {
                        modelo.insertarCirujano(vista.txtNombreCirujano.getText(),vista.txtApellidosCirujano.getText(),
                                vista.txtDniCirujano.getText(), Long.parseLong(vista.txtTelefonoCirujano.getText()),
                                vista.txtDireccionCirujano.getText(), vista.dateFechaNacCirujano.getDate(),
                                (String)vista.cbEspecialidadCirujano.getSelectedItem());

                        if(vista.txtDniCirujano.getText().length()!=9){
                            Util.showErrorAlert("El campo del DNI exige 9 carácteres");
                        }
                        refrescarCirujanos();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tableCirujanos.clearSelection();
                }
                borrarCamposCirujano();
            }
                break;
            case "ModificarCirujanos":{
                try {
                    if (comprobarCirujanoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tableCirujanos.clearSelection();
                    } else {
                        modelo.modificarCirujano(vista.txtNombreCirujano.getText(),vista.txtApellidosCirujano.getText(),
                                vista.txtDniCirujano.getText(), Long.parseLong(vista.txtTelefonoCirujano.getText()),
                                vista.txtDireccionCirujano.getText(), vista.dateFechaNacCirujano.getDate(),
                                (String)vista.cbEspecialidadCirujano.getSelectedItem(),
                                Integer.parseInt((String) vista.tableCirujanos.getValueAt(vista.tableCirujanos.getSelectedRow(), 0)));

                        if(vista.txtDniCirujano.getText().length()!=9){
                            Util.showErrorAlert("El campo del DNI exige 9 carácteres");
                        }
                        refrescarCirujanos();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tableCirujanos.clearSelection();
                }
                borrarCamposCirujano();
            }
                break;
            case "EliminarCirujanos":
                modelo.borrarCirujano(Integer.parseInt((String) vista.tableCirujanos.getValueAt(vista.tableCirujanos.getSelectedRow(), 0)));
                borrarCamposCirujano();
                refrescarCirujanos();
                break;
            case "ListarCirujanos":
                refrescarCirujanos();
                break;
            case "BuscarCirujanos":
                try {
                    if(vista.txtBuscarCirujano.getText().equalsIgnoreCase("")) {
                        cargarFilasCirujanos(modelo.buscarCirujano(""));
                        vista.txtBuscarCirujano.setText("");
                    }
                    else {
                        cargarFilasCirujanos(modelo.buscarCirujano(vista.txtBuscarCirujano.getText()));
                        vista.txtBuscarCirujano.setText("");
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                borrarCamposCirujano();
                vista.lblAccionCirujano.setText("Resultados Encontrados");
                vista.lblAccionCirujano.setForeground(new Color(31, 138, 19));

                break;
            case "AñadirOperaciones":{
                try {
                    if (comprobarOperacionVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tableOperacion.clearSelection();
                    } else {
                        modelo.insertarOperacion(vista.txtNombreOperacion.getText(),vista.txtDescripcionOperacion.getText(),
                                (String) vista.cbSalaOperacion.getSelectedItem(), vista.dateFechaOperacion.getDate(),
                                Integer.parseInt(vista.txtDuraccionOperacion.getText()),(String)vista.cbOrganoOperacion.getSelectedItem(),
                                (String)vista.cbAnestesiaOperacion.getSelectedItem(),(String)vista.cbCirujanoOperacion.getSelectedItem(),
                                (String)vista.cbPacienteOperacion.getSelectedItem(),(String)vista.cbDonanteOperacion.getSelectedItem());
                        refrescarOperaciones();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tableOperacion.clearSelection();
                }
                borrarCamposOperacion();
            }
                break;
            case "ModificarOperaciones":{
                try {
                    if (comprobarOperacionVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tableOperacion.clearSelection();
                    } else {
                        modelo.modificarOperacion(vista.txtNombreOperacion.getText(),vista.txtDescripcionOperacion.getText(),
                                (String) vista.cbSalaOperacion.getSelectedItem(), vista.dateFechaOperacion.getDate(),
                                Integer.parseInt(vista.txtDuraccionOperacion.getText()),(String)vista.cbOrganoOperacion.getSelectedItem(),
                                (String)vista.cbAnestesiaOperacion.getSelectedItem(),(String)vista.cbCirujanoOperacion.getSelectedItem(),
                                (String)vista.cbPacienteOperacion.getSelectedItem(),(String)vista.cbDonanteOperacion.getSelectedItem(),
                                Integer.parseInt((String) vista.tableOperacion.getValueAt(vista.tableOperacion.getSelectedRow(), 0)));
                        refrescarOperaciones();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tableOperacion.clearSelection();
                }
                borrarCamposOperacion();
            }
                break;
            case "EliminarOperaciones":
                modelo.borrarOperacion(Integer.parseInt((String) vista.tableOperacion.getValueAt(vista.tableOperacion.getSelectedRow(), 0)));
                borrarCamposOperacion();
                refrescarOperaciones();
                break;
            case "ListarOperaciones":
                refrescarOperaciones();
                break;
            case "BuscarOperaciones":
                try {
                    if(vista.txtBuscarOperacion.getText().equalsIgnoreCase("")) {
                        cargarFilasOperaciones(modelo.buscarOperacion(""));
                        vista.txtBuscarOperacion.setText("");
                    }
                    else {
                        cargarFilasOperaciones(modelo.buscarOperacion(vista.txtBuscarOperacion.getText()));
                        vista.txtBuscarOperacion.setText("");
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                borrarCamposOperacion();
                vista.lblAccionOperacion.setText("Resultados Encontrados");
                vista.lblAccionOperacion.setForeground(new Color(31, 138, 19));
                break;
            case "Limpiar Tabla O":
                try {
                    modelo.vaciarTablaOperaciones();
                    int numDatos = vista.dtmOperaciones.getRowCount();
                    for (int i = 0; i < numDatos; i++) {
                        vista.dtmOperaciones.removeRow(0);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Limpiar Tabla P":
                try {
                    modelo.vaciarTablaPacientes();
                    int numDatos = vista.dtmPacientes.getRowCount();
                    for (int i = 0; i < numDatos; i++) {
                        vista.dtmPacientes.removeRow(0);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Limpiar Tabla C":
                try {
                    modelo.vaciarTablaCirujanos();
                    int numDatos = vista.dtmCirujanos.getRowCount();
                    for (int i = 0; i < numDatos; i++) {
                        vista.dtmCirujanos.removeRow(0);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Limpiar Tabla D":
                try {
                    modelo.vaciarTablaDonantes();
                    int numDatos = vista.dtmDonantes.getRowCount();
                    for (int i = 0; i < numDatos; i++) {
                        vista.dtmDonantes.removeRow(0);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Exit":
                vista.frame.dispose();
                Login login=new Login();
                break;
            case "Conectar":
                modelo.conectar();
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
                break;
            default:
                throw new IllegalStateException("Error value: " + command);
        }
        }

    /**
     * Método que comprueba si los campos del paciente están vacios
     * @return vista.txtNombrePaciente.getText().isEmpty()||
     *                 vista.txtApellidoPaciente.getText().isEmpty()||
     *                 vista.txtDniPaciente.getText().isEmpty()||
     *                 vista.txtTelefonoPaciente.getText().isEmpty()||
     *                 vista.txtDireccionPaciente.getText().isEmpty()||
     *                 vista.dateFechaNacPaciente.getText().isEmpty()||
     *                 vista.cbGrupoSangPaciente.getSelectedIndex() == -1 ||
     *                 vista.cbOrganoPendPaciente.getSelectedIndex()== -1
     */
    private boolean comprobarPacienteVacio() {
        return  vista.txtNombrePaciente.getText().isEmpty()||
                vista.txtApellidoPaciente.getText().isEmpty()||
                vista.txtDniPaciente.getText().isEmpty()||
                vista.txtTelefonoPaciente.getText().isEmpty()||
                vista.txtDireccionPaciente.getText().isEmpty()||
                vista.dateFechaNacPaciente.getText().isEmpty()||
                vista.cbGrupoSangPaciente.getSelectedIndex() == -1 ||
                vista.cbOrganoPendPaciente.getSelectedIndex()== -1;

    }

    /**
     * Método que comprueba si los campos del donante están vacios
     * @return vista.txtNombreDonante.getText().isEmpty()||
     *                 vista.txtApellidosDonante.getText().isEmpty()||
     *                 vista.txtDniDonante.getText().isEmpty()||
     *                 vista.txtTelefonoDonante.getText().isEmpty()||
     *                 vista.txtDireccionDonante.getText().isEmpty()||
     *                 vista.dateFechaNacDonante.getText().isEmpty()||
     *                 vista.cbGrupoSangDonante.getSelectedIndex() == -1 ||
     *                 vista.cbOrganoDonante.getSelectedIndex() == -1 ||
     *                 vista.cbTipoDonante.getSelectedIndex() == -1
     */
    private boolean comprobarDonanteVacio() {
        return  vista.txtNombreDonante.getText().isEmpty()||
                vista.txtApellidosDonante.getText().isEmpty()||
                vista.txtDniDonante.getText().isEmpty()||
                vista.txtTelefonoDonante.getText().isEmpty()||
                vista.txtDireccionDonante.getText().isEmpty()||
                vista.dateFechaNacDonante.getText().isEmpty()||
                vista.cbGrupoSangDonante.getSelectedIndex() == -1 ||
                vista.cbOrganoDonante.getSelectedIndex() == -1 ||
                vista.cbTipoDonante.getSelectedIndex() == -1;
    }

    /**
     * Método que comprueba si los campos del cirujano están vacios
     * @return vista.txtNombreCirujano.getText().isEmpty()||
     *                 vista.txtApellidosCirujano.getText().isEmpty()||
     *                 vista.txtDniCirujano.getText().isEmpty()||
     *                 vista.txtTelefonoCirujano.getText().isEmpty()||
     *                 vista.txtDireccionCirujano.getText().isEmpty()||
     *                 vista.dateFechaNacCirujano.getText().isEmpty()||
     *                 vista.cbEspecialidadCirujano.getSelectedIndex() == -1
     */
    private boolean comprobarCirujanoVacio() {
        return  vista.txtNombreCirujano.getText().isEmpty()||
                vista.txtApellidosCirujano.getText().isEmpty()||
                vista.txtDniCirujano.getText().isEmpty()||
                vista.txtTelefonoCirujano.getText().isEmpty()||
                vista.txtDireccionCirujano.getText().isEmpty()||
                vista.dateFechaNacCirujano.getText().isEmpty()||
                vista.cbEspecialidadCirujano.getSelectedIndex() == -1;
    }

    /**
     * Método que comprueba si los campos de operacion están vacios
     * @return vista.txtNombreOperacion.getText().isEmpty()||
     *                 vista.txtDescripcionOperacion.getText().isEmpty()||
     *                 vista.cbSalaOperacion.getSelectedIndex() == -1||
     *                 vista.dateFechaOperacion.getText().isEmpty()||
     *                 vista.txtDuraccionOperacion.getText().isEmpty()||
     *                 vista.cbOrganoOperacion.getSelectedIndex() == -1||
     *                 vista.cbAnestesiaOperacion.getSelectedIndex() == -1||
     *                 vista.cbCirujanoOperacion.getSelectedIndex() == -1||
     *                 vista.cbPacienteOperacion.getSelectedIndex() == -1||
     *                 vista.cbDonanteOperacion.getSelectedIndex() == -1
     */
    private boolean comprobarOperacionVacio() {
        return  vista.txtNombreOperacion.getText().isEmpty()||
                vista.txtDescripcionOperacion.getText().isEmpty()||
                vista.cbSalaOperacion.getSelectedIndex() == -1||
                vista.dateFechaOperacion.getText().isEmpty()||
                vista.txtDuraccionOperacion.getText().isEmpty()||
                vista.cbOrganoOperacion.getSelectedIndex() == -1||
                vista.cbAnestesiaOperacion.getSelectedIndex() == -1||
                vista.cbCirujanoOperacion.getSelectedIndex() == -1||
                vista.cbPacienteOperacion.getSelectedIndex() == -1||
                vista.cbDonanteOperacion.getSelectedIndex() == -1;
    }

    /**
     * Método que borra los campos de texto de paciente
     */
    private void borrarCamposPaciente() {
        vista.txtNombrePaciente.setText("");
        vista.txtApellidoPaciente.setText("");
        vista.txtDniPaciente.setText("");
        vista.txtTelefonoPaciente.setText("");
        vista.txtDireccionPaciente.setText("");
        vista.dateFechaNacPaciente.setText("");
        vista.cbGrupoSangPaciente.setSelectedIndex(-1);
        vista.cbOrganoPendPaciente.setSelectedIndex(-1);
    }
    /**
     * Método que borra los campos de texto de donante
     */
    private void borrarCamposDonante() {
        vista.txtNombreDonante.setText("");
        vista.txtApellidosDonante.setText("");
        vista.txtDniDonante.setText("");
        vista.txtTelefonoDonante.setText("");
        vista.txtDireccionDonante.setText("");
        vista.dateFechaNacDonante.setText("");
        vista.cbGrupoSangDonante.setSelectedIndex(-1);
        vista.cbOrganoDonante.setSelectedIndex(-1);
        vista.cbTipoDonante.setSelectedIndex(-1);
    }
    /**
     * Método que borra los campos de texto de cirujano
     */
    private void borrarCamposCirujano() {
           vista.txtNombreCirujano.setText("");
           vista.txtApellidosCirujano.setText("");
           vista.txtDniCirujano.setText("");
           vista.txtTelefonoCirujano.setText("");
           vista.txtDireccionCirujano.setText("");
           vista.dateFechaNacCirujano.setText("");
           vista.cbEspecialidadCirujano.setSelectedIndex(-1);
    }
    /**
     * Método que borra los campos de texto de operacion
     */
    private void borrarCamposOperacion() {
           vista.txtNombreOperacion.setText("");
           vista.txtDescripcionOperacion.setText("");
           vista.cbSalaOperacion.setSelectedIndex(-1);
           vista.dateFechaOperacion.setText("");
           vista.txtDuraccionOperacion.setText("");
           vista.cbOrganoOperacion.setSelectedIndex(-1);
           vista.cbAnestesiaOperacion.setSelectedIndex(-1);
           vista.cbCirujanoOperacion.setSelectedIndex(-1);
           vista.cbPacienteOperacion.setSelectedIndex(-1);
           vista.cbDonanteOperacion.setSelectedIndex(-1);
    }

    /**
     * Metodo que carga las filas de la tabla de paciente
     * @param resultSet de tipo ResultSet
     * @throws SQLException
     */
    private void cargarFilasPaciente(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[9];
        vista.dtmPacientes.setRowCount(0);

        while (resultSet.next()) {
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);
            fila[6] = resultSet.getObject(7);
            fila[7] = resultSet.getObject(8);
            fila[8] = resultSet.getObject(9);

            vista.dtmPacientes.addRow(fila);
        }
        if (resultSet.last()) {
            vista.lblAccionPacientes.setVisible(true);
            vista.lblAccionPacientes.setText(resultSet.getRow() + " filas cargadas");
        }
    }
    /**
     * Metodo que carga las filas de la tabla de donante
     * @param resultSet de tipo ResultSet
     * @throws SQLException
     */
    private void cargarFilasDonantes(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[9];
        vista.dtmDonantes.setRowCount(0);

        while (resultSet.next()) {
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);
            fila[6] = resultSet.getObject(7);
            fila[7] = resultSet.getObject(8);
            fila[8] = resultSet.getObject(9);

            vista.dtmDonantes.addRow(fila);
        }
        if (resultSet.last()) {
            vista.lblAccionDonante.setVisible(true);
            vista.lblAccionDonante.setText(resultSet.getRow() + " filas cargadas");
        }
    }
    /**
     * Metodo que carga las filas de la tabla de cirujano
     * @param resultSet de tipo ResultSet
     * @throws SQLException
     */
    private void cargarFilasCirujanos(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[8];
        vista.dtmCirujanos.setRowCount(0);

        while (resultSet.next()) {
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);
            fila[6] = resultSet.getObject(7);
            fila[7] = resultSet.getObject(8);


            vista.dtmCirujanos.addRow(fila);
        }
        if (resultSet.last()) {
            vista.lblAccionCirujano.setVisible(true);
            vista.lblAccionCirujano.setText(resultSet.getRow() + " filas cargadas");
        }
    }
    /**
     * Metodo que carga las filas de la tabla de operacion
     * @param resultSet de tipo ResultSet
     * @throws SQLException
     */
    private void cargarFilasOperaciones(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[8];
        vista.dtmOperaciones.setRowCount(0);

        while (resultSet.next()) {
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);
            fila[6] = resultSet.getObject(7);
            fila[7] = resultSet.getObject(8);


            vista.dtmOperaciones.addRow(fila);
        }
        if (resultSet.last()) {
            vista.lblAccionOperacion.setVisible(true);
            vista.lblAccionOperacion.setText(resultSet.getRow() + " filas cargadas");
        }
    }

    /**
     * Metodo implementado por ListSelectionListener, que sirve para señalar un registro determinado
     * @param e de tipo ListSelectionEvent
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.tablePacientes.getSelectionModel())) {
                int row = vista.tablePacientes.getSelectedRow();
                vista.txtNombrePaciente.setText(String.valueOf(vista.tablePacientes.getValueAt(row, 1)));
                vista.txtApellidoPaciente.setText(String.valueOf(vista.tablePacientes.getValueAt(row, 2)));
                vista.txtDniPaciente.setText(String.valueOf(vista.tablePacientes.getValueAt(row, 3)));
                vista.txtTelefonoPaciente.setText(String.valueOf(vista.tablePacientes.getValueAt(row, 4)));
                vista.txtDireccionPaciente.setText(String.valueOf(vista.tablePacientes.getValueAt(row, 5)));
                vista.dateFechaNacPaciente.setDate((Date.valueOf(String.valueOf(vista.tablePacientes.getValueAt(row, 6)))).toLocalDate());
                vista.cbGrupoSangPaciente.setSelectedItem(String.valueOf(vista.tablePacientes.getValueAt(row, 7)));
                vista.cbOrganoPendPaciente.setSelectedItem(String.valueOf(vista.tablePacientes.getValueAt(row, 8)));
            } else if (e.getSource().equals(vista.tableDonantes.getSelectionModel())) {
                int row = vista.tableDonantes.getSelectedRow();
                vista.txtNombreDonante.setText(String.valueOf(vista.tableDonantes.getValueAt(row, 1)));
                vista.txtApellidosDonante.setText(String.valueOf(vista.tableDonantes.getValueAt(row, 2)));
                vista.txtDniDonante.setText(String.valueOf(vista.tableDonantes.getValueAt(row, 3)));
                vista.txtTelefonoDonante.setText(String.valueOf(vista.tableDonantes.getValueAt(row, 4)));
                vista.txtDireccionDonante.setText(String.valueOf(vista.tableDonantes.getValueAt(row, 5)));
                vista.dateFechaNacDonante.setDate((Date.valueOf(String.valueOf(vista.tableDonantes.getValueAt(row, 6)))).toLocalDate());
                vista.cbGrupoSangDonante.setSelectedItem(String.valueOf(vista.tableDonantes.getValueAt(row, 7)));
                vista.cbOrganoDonante.setSelectedItem(String.valueOf(vista.tableDonantes.getValueAt(row, 8)));
                vista.cbTipoDonante.setSelectedItem(String.valueOf(vista.tableDonantes.getValueAt(row, 9)));
            } else if (e.getSource().equals(vista.tableCirujanos.getSelectionModel())) {
                int row = vista.tableCirujanos.getSelectedRow();
                vista.txtNombreCirujano.setText(String.valueOf(vista.tableCirujanos.getValueAt(row, 1)));
                vista.txtApellidosCirujano.setText(String.valueOf(vista.tableCirujanos.getValueAt(row, 2)));
                vista.txtDniCirujano.setText(String.valueOf(vista.tableCirujanos.getValueAt(row, 3)));
                vista.txtTelefonoCirujano.setText(String.valueOf(vista.tableCirujanos.getValueAt(row, 4)));
                vista.txtDireccionCirujano.setText(String.valueOf(vista.tableCirujanos.getValueAt(row, 5)));
                vista.dateFechaNacCirujano.setDate((Date.valueOf(String.valueOf(vista.tableCirujanos.getValueAt(row, 6)))).toLocalDate());
                vista.cbEspecialidadCirujano.setSelectedItem(String.valueOf(vista.tableCirujanos.getValueAt(row, 7)));

            } else if (e.getSource().equals(vista.tableOperacion.getSelectionModel())) {
            int row = vista.tableOperacion.getSelectedRow();
                vista.txtNombreOperacion.setText(String.valueOf(vista.tableOperacion.getValueAt(row, 1)));
                vista.txtDescripcionOperacion.setText(String.valueOf(vista.tableOperacion.getValueAt(row, 2)));
                vista.cbSalaOperacion.setSelectedItem(String.valueOf(vista.tableOperacion.getValueAt(row, 3)));
                vista.dateFechaOperacion.setDate((Date.valueOf(String.valueOf(vista.tableOperacion.getValueAt(row, 4)))).toLocalDate());
                vista.txtDuraccionOperacion.setText(String.valueOf(vista.tableOperacion.getValueAt(row, 5)));
                vista.cbOrganoOperacion.setSelectedItem(String.valueOf(vista.tableOperacion.getValueAt(row, 6)));
                vista.cbAnestesiaOperacion.setSelectedItem(String.valueOf(vista.tableOperacion.getValueAt(row, 7)));
                vista.cbCirujanoOperacion.setSelectedItem(String.valueOf(vista.tableOperacion.getValueAt(row, 8)));
                vista.cbPacienteOperacion.setSelectedItem(String.valueOf(vista.tableOperacion.getValueAt(row, 9)));
                vista.cbDonanteOperacion.setSelectedItem(String.valueOf(vista.tableOperacion.getValueAt(row, 10)));

        }else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.tablePacientes.getSelectionModel())) {
                    borrarCamposPaciente();
                } else if (e.getSource().equals(vista.tableDonantes.getSelectionModel())) {
                    borrarCamposDonante();
                } else if (e.getSource().equals(vista.tableCirujanos.getSelectionModel())) {
                    borrarCamposCirujano();
                }else if (e.getSource().equals(vista.tableOperacion.getSelectionModel())) {
                    borrarCamposOperacion();
                }
            }
        }

    }

    /**
     * Método implementado por WindowListener, que se invoca cuando el usuario intenta cerrar la ventana
     * desde el menú del sistema de la ventana
     * @param e de tipo WindowEvent
     */
    @Override
    public void windowClosing (WindowEvent e){
        System.exit(0);
    }

    @Override
    public void windowOpened (WindowEvent e){

        }
    @Override
    public void windowClosed (WindowEvent e){

    }
    @Override
    public void windowIconified (WindowEvent e){

        }
    @Override
    public void windowDeiconified (WindowEvent e){

        }
    @Override
    public void windowActivated (WindowEvent e){
        }
    @Override
    public void windowDeactivated (WindowEvent e){

        }
    @Override
    public void itemStateChanged(ItemEvent e) {

    }


}



