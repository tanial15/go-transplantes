CREATE database if not exists gotrasplante;
--
use gotrasplante;
--
CREATE TABLE IF NOT EXISTS pacientes(
id int primary key auto_increment,
nombre varchar(50) not null,
apellido varchar(50) not null,
dni varchar(9) not null,
telefono long,
direccion varchar(150) not null,
fechaNac date,
grupoSanguineo varchar(20),
organoPendiente varchar(50)
);
--
CREATE TABLE IF NOT EXISTS donantes(
id int primary key auto_increment,
nombre varchar(50) not null,
apellido varchar(50) not null,
dni varchar(9) not null,
telefono long,
direccion varchar(150) not null,
fechaNac date,
grupoSanguineo varchar(20),
organo varchar(50),
tipoDonante varchar(50)
);
--
CREATE TABLE IF NOT EXISTS cirujanos(
id int primary key auto_increment,
nombre varchar(50) not null,
apellido varchar(50) not null,
dni varchar(9) not null,
telefono long,
direccion varchar(150) not null,
fechaNac date,
especialidad varchar(50)
);
--
CREATE TABLE IF NOT EXISTS operaciones(
id int primary key auto_increment,
nombre varchar(50) not null,
descripcion varchar(150),
sala varchar(50) not null,
fecha date,
duracion int,
organo varchar(50),
tipoAnestesia varchar(100),
id_cirujano INT NOT NULL,
id_paciente INT NOT NULL,
id_donante INT NOT NULL

);
--
alter table operaciones
add foreign key (id_cirujano) references cirujanos(id),
add foreign key (id_paciente) references pacientes(id),
add foreign key (id_donante) references donantes(id);
