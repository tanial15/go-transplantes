package com.tlopez.goTrasplantes.base;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 * Clase Database, que conecta con la base de datos.
 * @author Tania López
 */
public class Database {
    /**
     * Método de tipo Connection, que conecta con la base de datos de mysql
     * @return conexion
     */
    public Connection conectar(){
        Connection conexion=null;
        String url= "jdbc:mysql://127.0.0.1:3306/gotrasplante?user=root&password=";
        try {
            conexion=DriverManager.getConnection(url);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conexion;
    }
}
