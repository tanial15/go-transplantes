package com.tlopez.goTrasplantes.gui;

import com.tlopez.goTrasplantes.base.Database;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

/**
 * Clase Consultor que muestra la vista del tipo de usuario de consultor, es decir donde se encuentran los informes.
 * @author Tania López
 */
public class Consultor  {
    //Componentes de la interfaz
    public Connection conexion;
    public JPanel panel1;
    public JButton informe1Button;
    public JButton informe2Button;
    public JButton informe3Button;
    public JButton informe4Button;
    private JButton cerrarSesionButton;
    private JButton informe5button;
    private JLabel lblTitulo;
    public JFrame frame;

    /**
     * Constructor de Consultor, donde realizas la conexión con la base de datos, llamas al método que crea la ventana,
     * añade los action listeners de los botones, y añado los iconos de imágenes de dichos botones.
     */
    public Consultor() {
        this.conexion=new Database().conectar();
        initFrame();

        informe1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pintarPacientesInforme();
            }
        });
        informe2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pintarDonantesInforme();
            }
        });
        informe3Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pintarCirujanosInforme();
            }
        });
        informe4Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pintarOperacionesInforme();
            }
        });
        cerrarSesionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               Login login= new Login();
               frame.dispose();
            }
        });
        informe5button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pintarGraficoInforme();
            }
        });
        Image img=new ImageIcon("imagenes/close.png").getImage();
        ImageIcon img2= new ImageIcon(img.getScaledInstance(30,30,Image.SCALE_SMOOTH));
        cerrarSesionButton.setIcon(img2);
        Image img1=new ImageIcon("imagenes/grafico.png").getImage();
        ImageIcon imgGrafico= new ImageIcon(img1.getScaledInstance(32,32,Image.SCALE_SMOOTH));
        informe1Button.setIcon(imgGrafico);
        informe2Button.setIcon(imgGrafico);
        informe3Button.setIcon(imgGrafico);
        informe4Button.setIcon(imgGrafico);
        informe5button.setIcon(imgGrafico);
        ImageIcon imgGrafico2= new ImageIcon(img1.getScaledInstance(70,70,Image.SCALE_SMOOTH));
        lblTitulo.setIcon(imgGrafico2);

    }

    /**
     * Método que crea el pdf del informe de los pacientes
     */
    private void pintarPacientesInforme() {
        JasperPrint jasperPrint;
        try {
            Map<String, Object> parametros=new HashMap<String,Object>();
            parametros.put("titulo", "Pacientes en espera");
            parametros.put("autor", "Tania López");

            jasperPrint= JasperFillManager.fillReport("InformeTFG1.jasper",parametros , conexion);
            JRPdfExporter exp= new JRPdfExporter();
            exp.setExporterInput(new SimpleExporterInput(jasperPrint));
            exp.setExporterOutput(new SimpleOutputStreamExporterOutput("PacientesEnEspera.pdf"));
            SimplePdfExporterConfiguration conf= new SimplePdfExporterConfiguration();
            exp.setConfiguration(conf);
            exp.exportReport();

            JasperViewer jasperViewer= new JasperViewer(jasperPrint,false);//cuando se mete al informe no se me cierra la aplicacion
            jasperViewer.setVisible(true);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    /**
     * Método que crea el pdf del informe de los donantes
     */
    private void pintarDonantesInforme() {
        JasperPrint jasperPrint;
        try {
            Map<String, Object> parametros=new HashMap<String,Object>();
            parametros.put("titulo", "Donantes");
            parametros.put("autor", "Tania López");

            jasperPrint= JasperFillManager.fillReport("InformeTFG2.jasper",parametros , conexion);
            JRPdfExporter exp= new JRPdfExporter();
            exp.setExporterInput(new SimpleExporterInput(jasperPrint));
            exp.setExporterOutput(new SimpleOutputStreamExporterOutput("Donantes.pdf"));
            SimplePdfExporterConfiguration conf= new SimplePdfExporterConfiguration();
            exp.setConfiguration(conf);
            exp.exportReport();

            JasperViewer jasperViewer= new JasperViewer(jasperPrint,false);//cuando se mete al informe no se me cierra la aplicacion
            jasperViewer.setVisible(true);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    /**
     * Método que crea el pdf del informe de los cirujanos
     */
    private void pintarCirujanosInforme() {
        JasperPrint jasperPrint;
        try {
            Map<String, Object> parametros=new HashMap<String,Object>();
            parametros.put("titulo", "Cirujanos");
            parametros.put("autor", "Tania López");

            jasperPrint= JasperFillManager.fillReport("InformeTFG3.jasper",parametros , conexion);
            JRPdfExporter exp= new JRPdfExporter();
            exp.setExporterInput(new SimpleExporterInput(jasperPrint));
            exp.setExporterOutput(new SimpleOutputStreamExporterOutput("Cirujanos.pdf"));
            SimplePdfExporterConfiguration conf= new SimplePdfExporterConfiguration();
            exp.setConfiguration(conf);
            exp.exportReport();

            JasperViewer jasperViewer= new JasperViewer(jasperPrint,false);//cuando se mete al informe no se me cierra la aplicacion
            jasperViewer.setVisible(true);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    /**
     * Método que crea el pdf del informe de las operaciones
     */
    private void pintarOperacionesInforme() {
        JasperPrint jasperPrint;
        try {
            Map<String, Object> parametros=new HashMap<String,Object>();
            parametros.put("titulo", "Operaciones citadas");
            parametros.put("autor", "Tania López");

            jasperPrint= JasperFillManager.fillReport("InformeTFG4.jasper",parametros , conexion);
            JRPdfExporter exp= new JRPdfExporter();
            exp.setExporterInput(new SimpleExporterInput(jasperPrint));
            exp.setExporterOutput(new SimpleOutputStreamExporterOutput("Operaciones.pdf"));
            SimplePdfExporterConfiguration conf= new SimplePdfExporterConfiguration();
            exp.setConfiguration(conf);
            exp.exportReport();

            JasperViewer jasperViewer= new JasperViewer(jasperPrint,false);//cuando se mete al informe no se me cierra la aplicacion
            jasperViewer.setVisible(true);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    /**
     * Método que crea el pdf del informe del gráfico del estado en el que se encuentra el donante
     */
    private void pintarGraficoInforme() {
        JasperPrint jasperPrint;
        try {
            Map<String, Object> parametros=new HashMap<String,Object>();
            parametros.put("titulo", "Estado de los donantes");
            parametros.put("autor", "Tania López");

            jasperPrint= JasperFillManager.fillReport("InformeTFGGrafico.jasper",parametros , conexion);
            JRPdfExporter exp= new JRPdfExporter();
            exp.setExporterInput(new SimpleExporterInput(jasperPrint));
            exp.setExporterOutput(new SimpleOutputStreamExporterOutput("EstadoDonantes.pdf"));
            SimplePdfExporterConfiguration conf= new SimplePdfExporterConfiguration();
            exp.setConfiguration(conf);
            exp.exportReport();

            JasperViewer jasperViewer= new JasperViewer(jasperPrint,false);//cuando se mete al informe no se me cierra la aplicacion
            jasperViewer.setVisible(true);

        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Método que crea la ventana de la vista del consultor
     */
    private void initFrame() {
        frame = new JFrame("GO-Trasplantes--> Vista del consultor");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setSize(new Dimension(864, 491));
        Image img1=new ImageIcon("imagenes/vacuna.png").getImage();
        frame.setIconImage(img1);
    }




}
